terraform {
  required_version = ">= 1.0.0, < 2.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

locals {
  tcp_proto = "tcp"
  all_ips = ["0.0.0.0/0"]
}

resource "aws_autoscaling_group" "asg" {
  name                 = var.cluster_name
  launch_configuration = aws_launch_configuration.aws_lc.name
  vpc_zone_identifier  = var.subnet_ids 
  target_group_arns    = var.target_group_arns 
  health_check_type    = var.health_check_type
  min_size             = var.min_instances 
  max_size             = var.max_instances

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
  }

  tag {
    key                 = "Name"
    value               = var.cluster_name 
    propagate_at_launch = true
  }
  dynamic "tag" {
    for_each = {
      for key, value in var.custom_tags:
      key => upper(value)
      if key != "Name"
    }
    content {
      value = tag.value
      key   = tag.key
      propagate_at_launch = true
    }
  }
  
  lifecycle {
    postcondition {
      condition = length(self.availability_zones) > 1
      error_message = "use more than one AZ for HA"
    }
  }
}
