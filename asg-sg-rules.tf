resource "aws_security_group" "instance" {
  name = "${var.cluster_name}-instance"
}

resource "aws_security_group_rule" "allow_instance_http_inbound" {
  type = "ingress"
  security_group_id = aws_security_group.instance.id
  from_port   = var.server_port
  to_port     = var.server_port
  protocol    = local.tcp_proto
  cidr_blocks = local.all_ips
}

#resource "aws_security_group_rule" "allow_all_outbound" {
#  type = "egress"
#  security_group_id = aws_security_group.alb-sg.id
#  from_port = local.any_port 
#  to_port   = local.any_port
#  protocol  = local.any_proto
#  cidr_blocks = local.all_ips
#}
