variable "subnet_ids" {
  description = "subnet ID's to deploy ASG to"
  type = list(string)
}

variable "cluster_name" {
  description = "name of the autoscaling group cluster"
  type = string
  default = "microservice-cluster"
}

variable "ami_id" {
  description = "AMI ID to use in ASG"
  type = string
}

variable "instance_type" {
  description = "instance type to use in ASG"
  type = string
  default = "t3.micro"
}

variable "min_instances" {
  description = "minimum number of instances in the autoscaling group"
  type = number
  validation {
    condition = var.min_instances > 0
    error_message = "ASG of 0 instances not allowed"
  }
  validation {
    condition = var.min_instances < 10
    error_message = "ASG should not have more than 10 instances to keep costs down"
  }
}

variable "max_instances" {
  description = "maximum number of instances in the autoscaling group"
  type = number
  default = 2 
}

variable "target_group_arns" {
  description = "the ARN's of the ELB target groups to register instances"
  type = list(string)
  default = []
}

variable "health_check_type" {
  description = "health check, must be EC2 or ELB"
  type = string
  default = "EC2"
}

variable "enable_schedules" {
  description = "if set to true, enable auto scaling schedule"
  type = bool
  default = false
}

variable "user_data" {
  description = "user data script to be run by cloud init"
  type = string
  default = null
}

variable "server_port" {
  description = "web server port"
  type = number
  default = 8080
}

variable "custom_tags" {
  description = "custom tags to set on the instances in the ASG"
  type = map(string)
  default = {}
}
