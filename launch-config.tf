resource "aws_launch_configuration" "aws_lc" {
  image_id        = var.ami_id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.instance.id]
  user_data       = var.user_data

  #for using a launch config with an ASG
  lifecycle {
    create_before_destroy = true
    #precondition {
    #  condition = data.aws_ec2_instance_type.instance.free_tier_eligible
    #  error_message = "${var.instance_type} is not part of the AWS free tier"
    #}
  }
}
