resource "aws_autoscaling_schedule" "scale_out_during_business_hours" {
  count = var.enable_schedules ? 1 : 0
  scheduled_action_name = "${var.cluster_name}-scale-out-during-business-hours"
  min_size = var.min_instances 
  max_size = var.max_instances
  desired_capacity = var.max_instances
  recurrence = "0 9 * * *"
  autoscaling_group_name = aws_autoscaling_group.asg.name
}

resource "aws_autoscaling_schedule" "scale_in_at_night" {
  count = var.enable_schedules ? 1 : 0
  scheduled_action_name = "${var.cluster_name}-scale-in-at-night"
  min_size = var.min_instances 
  max_size = var.max_instances
  desired_capacity = var.min_instances
  recurrence = "0 17 * * *"
  autoscaling_group_name = aws_autoscaling_group.asg.name
} 
